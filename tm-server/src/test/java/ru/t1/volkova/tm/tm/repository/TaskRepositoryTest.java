package ru.t1.volkova.tm.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TaskRepositoryTest extends AbstractSchemeTest {

    private static int NUMBER_OF_ENTRIES = 4;

    private static String USER_ID_1;

    @NotNull
    private final static List<TaskDTO> taskList = new ArrayList<>();

    @NotNull
    private static ITaskDTORepository getTaskDTORepository() {
        return context.getBean(ITaskDTORepository.class);
    }

    @NotNull
    private static IProjectDTORepository getProjectDTORepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    @NotNull
    private static IUserDTORepository getUserDTORepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull UserDTO user = new UserDTO();
        user.setFirstName("User");
        user.setLastName("User");
        user.setMiddleName("User");
        user.setEmail("user@mail.ru");
        user.setLogin("user");
        user.setPasswordHash("user");
        USER_ID_1 = user.getId();
        userRepository.save(user);
        createTasks();
    }

    private static void createTasks()  {
        @NotNull final IProjectDTORepository projectRepository = getProjectDTORepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        NUMBER_OF_ENTRIES = 4;
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(USER_ID_1);
        projectRepository.save(project);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull TaskDTO task = new TaskDTO();
            task.setName("task" + i);
            task.setDescription("Tdescription" + i);
            task.setUserId(USER_ID_1);
            task.setProjectId(project.getId());
            taskRepository.save(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddForUserId() {
        @NotNull final String name = "Test task";
        @NotNull final String description = "Test description";
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(USER_ID_1);
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        int expectedNumberOfEntries = taskRepository.countByUserId(USER_ID_1) + 1;
        taskRepository.save(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.countByUserId(USER_ID_1));
        @Nullable final TaskDTO actualTask = taskRepository.findByUserIdAndId(USER_ID_1, task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(USER_ID_1, actualTask.getUserId());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        @Nullable final List<TaskDTO> taskList = taskRepository.findAllByUserId(USER_ID_1);
        if (taskList != null)
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskList.size());
    }

    @Test
    public void testFindAllForUserNegative() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        @Nullable final List<TaskDTO> taskList = taskRepository.findAllByUserId(null);
        if (taskList != null) Assert.assertEquals(0, taskList.size());
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        for (@NotNull final TaskDTO taskDTO : taskList) {
            Assert.assertEquals(taskDTO, taskRepository.findByUserIdAndId(taskDTO.getUserId(), taskDTO.getId()));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        Assert.assertNull(taskRepository.findByUserIdAndId(null, taskList.get(1).getId()));
        Assert.assertNull(taskRepository.findByUserIdAndId(USER_ID_1, "NotExcitingId"));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        @Nullable final TaskDTO task = taskRepository.findByUserIdAndId(USER_ID_1, taskList.get(0).getId());
        taskRepository.deleteByUserIdAndId(USER_ID_1, task.getId());
        assertNull(taskRepository.findByUserIdAndId(USER_ID_1, taskList.get(0).getId()));
        taskList.remove(0);
        NUMBER_OF_ENTRIES = taskRepository.findAllByUserId(USER_ID_1).size();
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final IProjectDTORepository projectRepository = getProjectDTORepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        taskRepository.deleteByUserId(USER_ID_1);
        projectRepository.deleteByUserId(USER_ID_1);
        assertEquals(0, taskRepository.countByUserId(USER_ID_1));
        taskList.clear();
        createTasks();
    }

    @Test
    public void testRemoveAllForUserNegative() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        taskRepository.deleteByUserId("NotExcitingId");
        Assert.assertNotEquals(0, taskRepository.countByUserId(USER_ID_1));
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.countByUserId(USER_ID_1));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        @NotNull final IProjectDTORepository projectRepository = getProjectDTORepository();
        @NotNull final ProjectDTO project = Objects.requireNonNull(projectRepository.findAllByUserId(USER_ID_1)).get(0);
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserIdAndProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void findAllByProjectIdNegative() {
        @NotNull final ITaskDTORepository taskRepository = getTaskDTORepository();
        @NotNull final IProjectDTORepository projectRepository = getProjectDTORepository();
        @NotNull final ProjectDTO project = projectRepository.findAllByUserId(USER_ID_1).get(0);
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByUserIdAndProjectId(null, project.getId());
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
    }

}
