package ru.t1.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDTORepository extends IAbstractUserOwnedDTORepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable String userId, @NotNull String projectId);

}
