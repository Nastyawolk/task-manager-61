package ru.t1.volkova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.service.IDatabaseProperty;
import ru.t1.volkova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService, IDatabaseProperty {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeOut;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['database.username']}")
    public String databaseUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    public String databasePassword;

    @NotNull
    @Value("#{environment['database.url']}")
    public String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseRegionPrefix;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseProviderCfgFileResPath;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseRegionFactoryClass;

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

}
