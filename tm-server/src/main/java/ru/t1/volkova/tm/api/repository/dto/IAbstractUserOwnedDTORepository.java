package ru.t1.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;

import java.util.List;

@NoRepositoryBean
public interface IAbstractUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel> extends IAbstractDTORepository<M> {

    @Nullable
    List<M> findAllByUserId(@Nullable String userId);

     @Nullable
     M findByUserIdAndId(@Nullable String userId, @Nullable String id);

     int countByUserId(@Nullable String userId);

     @Transactional
     void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

     @Transactional
     void deleteByUserId(@Nullable String userId);

     @Transactional
     void deleteAllByUserId(@Nullable String userId);

}
