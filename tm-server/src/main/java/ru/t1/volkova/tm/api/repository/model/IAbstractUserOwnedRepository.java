package ru.t1.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

import java.util.List;
@NoRepositoryBean
public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @Nullable
    List<M> findAllByUserId(@Nullable String userId);

    @Nullable
    M findByUserIdAndId(@Nullable String userId, @Nullable String id);

    int countByUserId(@Nullable String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@Nullable String userId);

    void deleteAllByUserId(@Nullable String userId);

}
