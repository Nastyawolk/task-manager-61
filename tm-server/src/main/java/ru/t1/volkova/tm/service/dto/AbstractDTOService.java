package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.t1.volkova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.volkova.tm.api.service.dto.IAbstractDTOService;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

@Getter
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IAbstractDTORepository<M>> implements IAbstractDTOService<M> {

}
