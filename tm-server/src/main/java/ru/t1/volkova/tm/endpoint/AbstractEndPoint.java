package ru.t1.volkova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.volkova.tm.api.service.IAuthService;
import ru.t1.volkova.tm.api.service.IPropertyService;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.user.AccessDeniedException;
import ru.t1.volkova.tm.dto.model.SessionDTO;

@Getter
@Controller
@NoArgsConstructor
public abstract class AbstractEndPoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected IProjectDTOService projectService;

    @NotNull
    @Autowired
    protected ITaskDTOService taskService;

    @NotNull
    @Autowired
    protected IUserDTOService userService;

    @NotNull
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getAuthService().validateToken(token);
    }

}
