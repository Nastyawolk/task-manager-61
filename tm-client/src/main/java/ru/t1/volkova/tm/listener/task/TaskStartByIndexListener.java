package ru.t1.volkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class TaskStartByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Start task status by index.";

    @NotNull
    private static final String NAME = "task-start-by-index";

    @Override
    @EventListener(condition = "@taskStartByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
