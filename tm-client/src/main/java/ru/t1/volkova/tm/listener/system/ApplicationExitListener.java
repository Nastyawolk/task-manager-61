package ru.t1.volkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.service.ILoggerService;
import ru.t1.volkova.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @Nullable
    private static final String ARGUMENT = null;

    @NotNull
    private static final String DESCRIPTION = "Close application.";

    @NotNull
    private static final String NAME = "exit";

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        @NotNull final ILoggerService loggerService = getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @Override
    public @Nullable String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
