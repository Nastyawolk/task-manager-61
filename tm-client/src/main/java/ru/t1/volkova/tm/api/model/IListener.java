package ru.t1.volkova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.event.ConsoleEvent;

public interface IListener {

    void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception;

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @NotNull
    String getName();

}
