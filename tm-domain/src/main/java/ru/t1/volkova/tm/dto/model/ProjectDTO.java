package ru.t1.volkova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.model.IWBS;
import ru.t1.volkova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO extends AbstractUserOwnedDTOModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(nullable = false)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    public String toString() {
        return getName() + " | " +
                getId() + " | " +
                getDescription() + " | ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectDTO project = (ProjectDTO) o;
        return name.equals(project.name) &&
                description.equals(project.description) &&
                status.getDisplayname().equals(project.status.getDisplayname()) &&
                getId().equals(project.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}
